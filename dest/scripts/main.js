$(document).ready(function(){
  $('.portfolio__slider').slick({
    prevArrow: $('.portfolio__slider-arrow--prev'),
    nextArrow: $('.portfolio__slider-arrow--next')
  });

  // tabs
  var tabs = '.tabs',
      tabsLink = '.tabs__list-link',
      tabsLinkActiveClass = "tabs__list-link--active"
      tab = '.tab',
      tabActiveClass= 'tab--active';

  $(tabs).each(function(){
    var itemLink = $(this).find(tabsLink),
        itemsTab = $(this).find(tab);

    itemLink.click(function(e){
      e.preventDefault();
      var itemTab = $(this).attr('href');

      itemLink.removeClass(tabsLinkActiveClass);
      $(this).addClass(tabsLinkActiveClass);
      itemsTab.removeClass(tabActiveClass);
      $(itemTab).addClass(tabActiveClass);
    });
  });

  // .rate-list

  $('.rate-list ').slick({
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1
  });
});
